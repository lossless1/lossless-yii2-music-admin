<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `core_tags`.
 */
class m170205_142417_create_core_tags_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('core_tags', [
            'id_tag' => Schema::TYPE_PK,
            'name_tag' => Schema::TYPE_STRING." DEFAULT NULL",
            'created' => Schema::TYPE_TIMESTAMP
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('core_tags');
    }
}
