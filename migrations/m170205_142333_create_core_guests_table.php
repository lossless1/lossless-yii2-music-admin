<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `core_guests`.
 */
class m170205_142333_create_core_guests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('core_guests', [
            'id_guest' => Schema::TYPE_PK,
            'ip_guest' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'created' => Schema::TYPE_TIMESTAMP
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('core_guests');
    }
}
