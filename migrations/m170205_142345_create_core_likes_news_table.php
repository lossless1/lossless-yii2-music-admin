<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `core_likes_news`.
 */
class m170205_142345_create_core_likes_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('core_likes_news', [
            'id_news' => Schema::TYPE_PK,
            'id_user' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'created' => Schema::TYPE_TIMESTAMP
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('core_likes_news');
    }
}
