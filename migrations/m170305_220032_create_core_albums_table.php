<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `core_albums`.
 */
class m170305_220032_create_core_albums_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('core_albums', [
            'id_album' => Schema::TYPE_PK,
            'id_author' => Schema::TYPE_INTEGER,
            'name_album' => Schema::TYPE_STRING,
            'id_songs' => Schema::TYPE_STRING,
            'id_avatar' => Schema::TYPE_INTEGER,
            'created' => Schema::TYPE_TIMESTAMP,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('core_albums');
    }
}
