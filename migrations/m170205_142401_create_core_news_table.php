<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `core_likes_news`.
 */
class m170205_142401_create_core_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('core_news', [
            'id_news' => Schema::TYPE_PK,
            'id_category' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'id_author' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'id_tags' => Schema::TYPE_STRING." DEFAULT NULL",
            'id_songs' => Schema::TYPE_STRING." DEFAULT NULL",
            'id_avatar' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'likes'=> Schema::TYPE_INTEGER." DEFAULT NULL",
            'title' => Schema::TYPE_STRING." DEFAULT NULL",
            'content' => Schema::TYPE_STRING." DEFAULT NULL",
            'created' => Schema::TYPE_TIMESTAMP
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('core_news');
    }
}
