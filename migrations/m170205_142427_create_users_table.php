<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `users`.
 */
class m170205_142427_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id_user' => Schema::TYPE_PK,
            'user_login' => Schema::TYPE_STRING." DEFAULT NULL",
            'user_password' => Schema::TYPE_STRING." DEFAULT NULL",
            'user_email' => Schema::TYPE_STRING." DEFAULT NULL",
            'id_news_liked' => Schema::TYPE_STRING." DEFAULT NULL",
            'id_news_added' => Schema::TYPE_STRING." DEFAULT NULL",
            'avatar_path' => Schema::TYPE_STRING." DEFAULT NULL",
            'id_tags' => Schema::TYPE_STRING." DEFAULT NULL",
            'id_user_vk' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'id_user_google' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'id_user_facebook' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'id_user_twitter' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'created' => Schema::TYPE_TIMESTAMP,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
