<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `core_songs`.
 */
class m170205_142409_create_core_songs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('core_songs', [
            'id_song' => Schema::TYPE_PK,
            'id_author' => Schema::TYPE_INTEGER,
            'id_album' => Schema::TYPE_INTEGER,
            'filename' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_STRING,
            'song_name' => Schema::TYPE_STRING,
            'original_name' => Schema::TYPE_STRING,
            'crypt' => Schema::TYPE_STRING,
            'chrono' => Schema::TYPE_STRING,
            'likes' => Schema::TYPE_INTEGER,
            'created' => Schema::TYPE_TIMESTAMP,

        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('core_songs');
    }
}
