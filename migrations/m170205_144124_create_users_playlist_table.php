<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `users_playlist`.
 */
class m170205_144124_create_users_playlist_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users_playlist', [
            'id_user' => Schema::TYPE_PK,
            'id_news' => Schema::TYPE_INTEGER." DEFAULT NULL",
            'created' => Schema::TYPE_TIMESTAMP
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users_playlist');
    }
}
