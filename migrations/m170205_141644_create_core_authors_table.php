<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `core_authors`.
 */
class m170205_141644_create_core_authors_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('core_authors', [
            'id_author' => Schema::TYPE_PK,
            'author_name' => Schema::TYPE_STRING . " DEFAULT NULL",
            'id_albums' => Schema::TYPE_STRING . " DEFAULT NULL",
            'id_avatar' => Schema::TYPE_INTEGER . " DEFAULT NULL",
            'quantity_subs' => Schema::TYPE_INTEGER . " DEFAULT NULL",
            'created' => Schema::TYPE_TIMESTAMP
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('core_authors');
    }
}
