<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `core_avatars`.
 */
class m170215_103812_create_core_avatars_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('core_avatars', [
            'id_avatar' => Schema::TYPE_PK,
            'avatar_name' => Schema::TYPE_STRING,
            'avatar_fullname' => Schema::TYPE_STRING,
            'avatar_extension' => Schema::TYPE_STRING,
            'avatar_path' => Schema::TYPE_STRING,
            'avatar_crypt' => Schema::TYPE_STRING,
            'avatar_base64' => "longtext",
            'avatar_size' => Schema::TYPE_INTEGER,
            'created' => Schema::TYPE_TIMESTAMP,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('core_avatars');
    }
}
