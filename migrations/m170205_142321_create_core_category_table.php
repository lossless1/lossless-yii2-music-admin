<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `core_category`.
 */
class m170205_142321_create_core_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('core_category', [
            'id_category' => Schema::TYPE_PK,
            'category_name' => Schema::TYPE_STRING. " DEFAULT NULL",
            'created' => Schema::TYPE_TIMESTAMP
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('core_category');
    }
}
