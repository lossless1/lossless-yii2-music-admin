<?php
/**
 * Created by PhpStorm.
 * User: lossless
 * Date: 2/3/17
 * Time: 21:26
 */
?>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Music Web Admin <?= date('Y') ?></p>
    </div>
</footer>
