<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use \yii\bootstrap\ActiveForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/images/pic.png" type="image/png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php $this->beginContent('@app/views/layouts/header.php'); ?>
    <!-- You may need to put some content here -->
    <?php $this->endContent(); ?>
    <div class="container main-container">
        <div class="left-nav-menu col-xs-3">
            <div>
                <?php ActiveForm::begin(['id' => 'h-search', 'method' => 'post', 'action' => '/site/search']) ?>
                <span class="glyphicon glyphicon-search"></span>
                <?= Html::submitButton('Search', ['class' => 'left-nav-buttons']); ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div>
                <?php ActiveForm::begin(['id' => 'h-news', 'method' => 'post', 'action' => '/site/news']) ?>
                <span class="glyphicon glyphicon-fire"></span>
                <?= Html::submitButton('News', ['class' => 'left-nav-buttons','style'=>'text-decoration:line-through','disabled'=>'disabled']); ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div>
                <?php ActiveForm::begin(['id' => 'h-up-song', 'method' => 'post', 'action' => '/site/upload-song']) ?>
                <span class="glyphicon glyphicon-cloud-upload"></span>
                <?= Html::submitButton('Upload song', ['class' => 'left-nav-buttons']); ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div>
                <?php ActiveForm::begin(['id' => 'h-up-album', 'method' => 'post', 'action' => '/site/add-album']) ?>
                <span class="glyphicon glyphicon-cloud-upload"></span>
                <?= Html::submitButton('Create album', ['class' => 'left-nav-buttons']); ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div>
                <?php ActiveForm::begin(['id' => 'h-up-author', 'method' => 'post', 'action' => '/site/add-author']) ?>
                <span class="glyphicon glyphicon-cloud-upload"></span>
                <?= Html::submitButton('Create author', ['class' => 'left-nav-buttons']); ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div>
                <?php ActiveForm::begin(['id' => 'h-up-category', 'method' => 'post', 'action' => '/site/add-category']) ?>
                <span class="glyphicon glyphicon-cloud-upload"></span>
                <?= Html::submitButton('Create category', ['class' => 'left-nav-buttons','style'=>'text-decoration:line-through','disabled'=>'disabled']); ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div>
                <?php ActiveForm::begin(['id' => 'h-up-news', 'method' => 'post', 'action' => '/site/add-news']) ?>
                <span class="glyphicon glyphicon-cloud-upload"></span>
                <?= Html::submitButton('Create news', ['class' => 'left-nav-buttons','style'=>'text-decoration:line-through','disabled'=>'disabled']); ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div>
                <?php ActiveForm::begin(['id' => 'h-delete', 'method' => 'post', 'action' => '/site/delete']) ?>
                <span class="glyphicon glyphicon-floppy-remove"></span>
                <?= Html::submitButton('Delete', ['class' => 'left-nav-buttons']); ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div>
                <?php ActiveForm::begin(['id' => 'h-debug', 'method' => 'post', 'action' => '/site/debug']) ?>
                <span class="glyphicon glyphicon-compressed"></span>
                <?= Html::submitButton('Debug', ['class' => 'left-nav-buttons']); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="right-side-cont col-xs-9">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>
    <?php $this->beginContent('@app/views/layouts/footer.php'); ?>
    <!-- You may need to put some content here -->
    <?php $this->endContent(); ?>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
