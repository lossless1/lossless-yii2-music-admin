<?php
use yii\helpers\Html;
$this->title = 'Layout page';
$this->registerJsFile('@web/js/sendMusic');
$this->registerJsFile('@web/js/page_song');

?>
<div id="music">
    <p class='main-text-name'><?= Html::encode($this->title)?></p>

    Stranica s loisom!<br><br>
</div>
