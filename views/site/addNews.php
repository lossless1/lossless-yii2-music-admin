<?php
use yii\helpers\Html;
use kartik\widgets\Select2;

$this->title = "Create News";

//$this->registerJsFile('@web/js/add_news.js');

$this->registerCssFile('@web/css/bootstrap-fileinput-master/css/fileinput.css');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/canvas-to-blob.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/sortable.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/purify.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/fileinput.js');
$this->registerJsFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');

$this->registerCssFile('@web/css/create-pages.css');

?>
<table border="0" align="center">
    <p class='main-text-name'><?= Html::encode($this->title) ?></p>
    <div class="form-group">
        <div class="form-category">
            <?php
            echo Html::tag('label', Yii::t('app', 'Category:'), ['class' => 'control-label']);
            ?>
            <div>
                <?php
                echo Html::tag('span', Yii::t('app', 'Choose exist category or create new '), ['class' => 'small']);
                echo Html::a(Yii::t('app', ' here'), '/site/add-category', ['style' => 'font-size: 85%']);
                ?>
            </div>
            <?php
            echo Select2::widget([
                'name' => 'category-select',
                'id' => 'category',
                'data' => $data['category-list'],
                'disabled' => $data['category-opt']['disabled'],
                'options' => ['placeholder' => 'Select categories ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10
                ],
            ]);
            ?>
        </div>

        <div class="form-author">
            <?php
            echo Html::tag('label', Yii::t('app', 'Author:'), ['class' => 'control-label']);
            ?>
            <div>
                <?php
                echo Html::tag('span', Yii::t('app', 'Choose exist author or create new'), ['class' => 'small']);
                echo Html::a(Yii::t('app', ' here'), '/site/add-author', ['style' => 'font-size:85%']);
                ?>
            </div>
            <?php
            echo Select2::widget([
                'name' => 'author-select',
                'id' => 'author',
                'data' => $data['authors-list'],
                'disabled' => $data['authors-opt']['disabled'],
                'options' => ['placeholder' => 'Select authors ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10
                ],
            ]);
            ?>
        </div>
        <div class="form-tags">
            <?php
            echo Html::tag('label', Yii::t('app', 'Tags:'), ['class' => 'control-label']);
            ?>
            <div>
                <?php
                echo Html::tag('span', Yii::t('app', 'Choose exist tags or create new here'), ['class' => 'small']);
                ?>
            </div>
            <?php
            echo Select2::widget([
                'name' => 'tags-select',
                'id' => 'tags',
                'data' => $data['tags-list'],
                'maintainOrder' => true,
                'options' => ['placeholder' => 'Select tags ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'maximumInputLength' => 10
                ],
            ]);
            ?>
        </div>
        <div class="form-songs">
            <?php
            echo Html::tag('label', Yii::t('app', 'Songs:'), ['class' => 'control-label']);
            ?>
            <div>
                <?php
                echo Html::tag('span', Yii::t('app', 'Choose exist songs or create new '), ['class' => 'small']);
                echo Html::a(Yii::t('app', ' here'), '/site/upload-song', ['style' => 'font-size:85%']);
                ?>
            </div>
            <?php
            echo Select2::widget([
                'name' => 'songs-select',
                'id' => 'songs',
                'data' => $data['songs-list'],
                'disabled' => $data['songs-opt']['disabled'],
                'maintainOrder' => true,
                'options' => ['placeholder' => 'Select songs ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'maximumInputLength' => 10
                ],
            ]);
            ?>
        </div>

        <div class="form-picture">
            <?php
            echo Html::tag('label', Yii::t('app', 'Picture:'), ['class' => 'control-label']);
            ?>
            <div>
                <?php
                echo Html::tag('span', Yii::t('app', 'Upload your avatar for new.'), ['class' => 'small']);
                ?>
            </div>
            <input class="file-loading" id="avatar-input" name="avatar-input" type="file" data-show-preview="false">
        </div>

        <div class="form-title">
            <?php
            echo Html::tag('label', Yii::t('app', 'Title:'), ['class' => 'control-label']);
            ?>
            <div>
                <?php
                echo Html::tag('span', Yii::t('app', 'Input title for news for display on news page.'), ['class' => 'small pull-left']);
                ?>
            </div>
            <?php
            echo Html::tag('label', Yii::t('app', 'remain 50 symbols'), ['class' => 'control-label pull-right text-success', 'id' => 'remain-sym-title']);
            ?>
            <input class="form-control" id="title" type="text" placeholder="Input title...">
        </div>

        <div class="form-content">
            <?php
            echo Html::tag('label', Yii::t('app', 'Content:'), ['class' => 'control-label']);
            ?>
            <div>
                <?php
                echo Html::tag('span', Yii::t('app', 'Input content of songs for more details.'), ['class' => 'small pull-left']);
                ?>
            </div>
            <?php
            echo Html::tag('label', Yii::t('app', 'remain 255 symbols'), ['class' => 'control-label pull-right text-success', 'id' => 'remain-sym-content']);
            ?>
            <textarea class="form-control" id="content" rows="5" placeholder="Comment..."></textarea>
        </div>
        <div class="col-lg-12 text-center">
            <input class="btn btn-success btn-md" id="send" type="submit" value="submit">
        </div>
    </div>
</table>

<script>

    $("#content").keyup(function () {
        var content = $("#content");
        var maxTextContent = 255;
        var remainContent = $("#remain-sym-content");


        remainContent.html("remain " + (maxTextContent - content.val().length) + " symbols");
        if ((maxTextContent - content.val().length) < 0) {
            remainContent.removeClass('text-danger text-success').addClass('text-danger');
        } else {
            remainContent.removeClass('text-success text-danger').addClass('text-success');
        }
    });

    $("#title").keyup(function () {
        var title = $("#title");
        var maxTextTitle = 50;
        var remainTitle = $("#remain-sym-title");
        remainTitle.html("remain " + (maxTextTitle - title.val().length) + " symbols");
        if ((maxTextTitle - title.val().length) < 0) {
            remainTitle.removeClass('text-danger text-success').addClass('text-danger');
        } else {
            remainTitle.removeClass('text-success text-danger').addClass('text-success');
        }
    });
    $(document).ready(function () {

        $("#avatar-input").fileinput({
            maxFileSize: 2000,
            browseClass: "btn btn-success",
            browseLabel: "Pick Avatar",
            previewFileType: "image",
            allowedFileTypes: ["image"],
            allowedFileExtensions: ["png", "jpeg", "jpg"],

            previewFileIconSettings: {
                'png': '<i class="fa fa-file-photo-o text-warning"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            },
            previewFileExtSettings: {
                'png': function (ext) {
                    return ext.match(/(png|jpeg)$/i);
                },
                'jpg': function (ext) {
                    return ext.match(/(jpg)$/i);
                }
            },
            overwriteInitial: false,
            initialCaption: "Choose avatar...",
            showUpload: false

        });


        $("#send").click(function () {
            var category = $("#category").val();
            var author = $("#author").val();
            var tags = JSON.stringify($("#tags").val());
            var songs = JSON.stringify($("#songs").val());
            var avatar = $("#avatar-input");
            var title = $("#title").val();
            var content = $("#content").val();

            var form = new FormData();
            $.each(avatar[0].files, function (key, value) {
                form.append("avatar", value);
            });
            form.append("category", category);
            form.append("author", author);
            form.append("tags", tags);
            form.append("songs", songs);
            form.append("avatar", avatar);
            form.append("title", title);
            form.append("content", content);

            $("p.alert-success").remove();
            $("p.alert-danger").remove();
            $.ajax({
                url: "/save/save-news",
                data: form,
                type: "post",
                cache: false,
                contentType: false,
                processData: false
            }).done(function (response) {
                response = JSON.parse(response);
                console.log(response);
                if (response.result) {
                    $(".form-group").append("<p class='alert alert-success'>" + response.content + "</p>");
                    console.log(response.content);
                } else {
                    $(".form-group").append("<p class='alert alert-danger'>" + response.content + "</p>");
                    console.log(response.content);
                }
                setTimeout(
                    function () {
                        $('.alert').fadeOut('fast');
                    }, 3000);
            }).fail(function (error) {
                console.log(error.responseText);
            });
        });
    });
</script>