<?php
use yii\helpers\Html;

$this->registerJsFile('@web/js/user.js');
$this->title = "Personal cabinet";
?>

<div>

    <p class='main-text-name'><?= Html::encode($this->title) ?></p>

    <div id="cabinet">

        Личный кабинет. <br>
    </div>
</div>

