<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AppAsset;
$this->registerJsFile('@web/js/auth.js');
$this->registerJsFile('@web/js/reglog.js');
$this->title = "Login Page";
?>
<p class='main-text-name'><?= Html::encode($this->title)?></p>
<div>
    <?php $form = ActiveForm::begin(['id'=>'login-form',
        'options' => [
            'class' => 'col-lg-10',
        ],

        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-lg-4',
            ],
            'template' => "<div class='col-lg-5'>{label}</div><div class='col-lg-8'>{input}</div>\n<div class='col-lg-8'>{error}</div>",
            'labelOptions' => ['class'=>'col-lg-12 control-panel'],
        ]]);?>
    <?= $form->field($model,'username')->textInput(['autofocus'=>true])?>
    <?= $form->field($model,'password')->passwordInput();?>
    <?= Html::submitButton('Log In',['id'=>'Register','class'=>'btn btn-primary col-lg-3','action'=>'/site/search'])?>
    <?php ActiveForm::end();?>
</div>

<div id=\"name\"></div>
<div class=\"auth\">
    <div id=\"googleLg\"></div>
    <div id=\"facebookLg\"></div>
    <div id=\"vkLg\"></div>
    <div id=\"twitterLg\"></div>
</div>


