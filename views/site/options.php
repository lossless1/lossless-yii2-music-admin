<?php
use yii\helpers\Html;
$this->registerJsFile('@web/js/user_options.js');
$this->title = "Options";
?>
</br >
<p class='main-text-name'><?= Html::encode($this->title)?></p>

Rename user:
<input id="username_update" type="text"/></br >
<div id="error_username" style="color:red;"></div></br >
Rename email:
<input id="email_update" type="text"/></br >
<div id="error_email" style="color:red;"></div></br >
Avatar file:
<input id="avatar_update" type="file" accept="image/*"/></br ></br >
<button id="submit">Сохранить</button>
<button id="deleteuser">Удалиться</button>
<div id="info"></div>
