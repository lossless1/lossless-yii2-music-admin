<?php
use yii\helpers\Html;
use kartik\widgets\Select2;

$this->title = "Adding Author";

$this->registerCssFile('@web/css/bootstrap-fileinput-master/css/fileinput.css');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/canvas-to-blob.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/sortable.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/purify.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/fileinput.js');
$this->registerJsFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');
$this->registerCssFile('@web/css/author.css');
?>
<p class='main-text-name'><?= Html::encode($this->title) ?></p>

<div class="form-group">

    <div>
        <?php
        echo Html::tag('label', Yii::t('app', 'Author Name:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('p', Yii::t('app', 'You can input new name for author only.'), ['class' => 'small pull-left']);
            echo Html::tag('label', Yii::t('app', 'Max 30 symbols.'), ['class' => 'small pull-right text-success', 'id' => 'remain-sym-author']);
            echo Html::input('text', 'author', '', ['class' => 'form-control', 'id' => 'author']);
            ?>
        </div>
    </div>
    <div class="form-albums">
        <?php
        echo Html::tag('label', Yii::t('app', 'Select Albums:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('span', Yii::t('app', 'Choose exist albums or create new '), ['class' => 'small']);
            echo Html::a(Yii::t('app', ' here'), '/site/add-album', ['style' => 'font-size: 85%']);
            ?>
        </div>
        <?php
        echo Select2::widget([
            'name' => 'album-select',
            'id' => 'album',
            'data' => $data['album-list'],
            'disabled' => $data['album-opt']['disabled'],
            'maintainOrder' => true,
            'options' => ['placeholder' => 'Select album ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10
            ],
        ]);
        ?>
    </div>
    <div class="form-songs">
        <?php
        echo Html::tag('label', Yii::t('app', 'Select Songs:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('span', Yii::t('app', 'Choose exist songs or create new '), ['class' => 'small']);
            echo Html::a(Yii::t('app', ' here'), '/site/upload-song', ['style' => 'font-size: 85%']);
            ?>
        </div>
        <?php
        echo Select2::widget([
            'name' => 'songs-select',
            'id' => 'songs',
            'data' => $data['songs-list'],
            'disabled' => 'disabled',//$data['songs-opt']['disabled']
            'maintainOrder' => true,
            'options' => ['placeholder' => 'Disable for now', 'multiple' => true],//Select songs ...
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10
            ],
        ]);
        ?>
    </div>
    <div>
        <?php
        echo Html::tag('label', Yii::t('app', 'Avatar:'), ['class' => 'control-label']);
        echo Html::tag('p', Yii::t('app', 'Extension for image: img,jpeg,png.'), ['class' => 'small']);
        ?>
        <input class="file-loading" id="avatar" name="avatar-input" type="file" data-show-preview="false">
    </div>
    <div>
        <button class="btn btn-success btn-md" id="send" type="submit">
            <div>CREATE</div>
        </button>
    </div>

    <div id="info"></div>
</div>
<script>
    $(function () {
        $("#author").keyup(function () {
            var author = $("#author");
            var remain = $("#remain-sym-author");
            remain.html("Max " + (30 - author.val().length) + " symbols");
            if ((30 - author.val().length) < 0) {
                remain.removeClass('text-danger text-success').addClass('text-danger');
            } else {
                remain.removeClass('text-success text-danger').addClass('text-success');
            }
        });
        $("#avatar").fileinput({
            maxFileSize: 2000,
            browseClass: "btn btn-success",
            browseLabel: "Pick Avatar",
            previewFileType: "image",
            allowedFileTypes: ["image"],
            allowedFileExtensions: ["png", "jpeg", "jpg"],

            previewFileIconSettings: {
                'png': '<i class="fa fa-file-photo-o text-warning"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            },
            previewFileExtSettings: {
                'png': function (ext) {
                    return ext.match(/(png|jpeg)$/i);
                },
                'jpg': function (ext) {
                    return ext.match(/(jpg)$/i);
                }
            },
            overwriteInitial: false,
            initialCaption: "Choose avatar...",
            showUpload: false
        });
        $("#send").click(function () {
            var author = $("#author").val();
            var albums = JSON.stringify($("#album").val());
            var avatar = $("#avatar");

            var form = new FormData();
            $.each(avatar[0].files, function (key, value) {
                form.append("avatar", value);
            });
            form.append("author_name", author);
            form.append("albums_name", albums);

            $("p.alert-success").remove();
            $("p.alert-danger").remove();
            $.ajax({
                url: "/save/save-author",
                data: form,
                type: "post",
                cache: false,
                beforeSend: function () {
                    $("#send div").addClass("loader").html("");
                },
                contentType: false,
                processData: false
            }).done(function (response) {
                $("#send div").removeClass("loader").html("CREATE");
                response = JSON.parse(response);
                console.log(response);
                if (response.result) {
                    $(".form-group").append("<p class='alert alert-success'>" + response.content + "</p>");
                    console.log(response.content);
                } else {
                    $(".form-group").append("<p class='alert alert-danger'>" + response.content + "</p>");
                    console.log(response.content);
                }
                setTimeout(
                    function () {
                        $('.alert').fadeOut('fast');
                    }, 3000);
            }).fail(function (error) {
                console.log(error.responseText);
            });
        });
    })
</script>