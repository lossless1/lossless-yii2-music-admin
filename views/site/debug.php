<?php
use yii\helpers\Html;

$this->title = "Debug page";
//$this->registerJsFile('@web/js/debug');

?>
<div class="right-side-cont">
    <p class='main-text-name'><?= Html::encode($this->title) ?></p>

    <div id="info"></div>
</div>
<script>

</script>
