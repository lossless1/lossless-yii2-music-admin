<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->registerCssFile('@web/css/bootstrap-fileinput-master/css/fileinput.css');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/canvas-to-blob.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/sortable.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/purify.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/fileinput.js');
$this->registerJsFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');
$this->title = 'Upload song user page';
//$this->registerJsFile('@web/js/sendFile');
?>

<div>
    <p class='main-text-name'><?= Html::encode($this->title) ?></p>


    <div class="form-group">
        <input id="input-24" name="input24" type="file" multiple class="file-loading">
    </div>

</div>
<script>
    $(document).ready(function () {
        $("#input-24").fileinput({
            previewFileType: "audio",
            allowedFileExtensions: ["mp3"],
            browseClass: "btn btn-success",
            browseLabel: "Pick Songs",
            browseIcon: "<i class=\"glyphicon glyphicon-music\"></i>",
            removeClass: "btn btn-danger",
            removeLabel: "Delete",
            removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i>",
            uploadUrl: "/save/save-song",
            uploadClass: "btn btn-info",
            uploadLabel: "Upload",
            uploadIcon: "<i class=\"glyphicon glyphicon-upload\"></i>",
            previewSettings: {
                audio: {width: "213px", height: "80px"}
            },
            maxFileSize: 10000,
            previewFileIconSettings: {
                'mp3': '<i class="fa fa-file-audio-o text-warning"></i>'
            },
            previewFileExtSettings: {
                'mp3': function (ext) {
                    return ext.match(/(mp3|wav)$/i);
                }
            },
            overwriteInitial: false,
            initialCaption: "Choose song files..."
        });
    });
</script>