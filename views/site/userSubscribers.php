<?php
use yii\helpers\Html;
$this->registerJsFile('@web/js/page_subscriptions');
$this->title = "Subscriptions";

?>
<div>
    <p class='main-text-name'><?= Html::encode($this->title)?></p>
    <div id="subscriptions"></div>
</div>

