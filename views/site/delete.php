<?php
use yii\helpers\Html;
use kartik\widgets\Select2;

$this->title = "Delete page";

$this->registerJsFile('@web/js/select2/dist/js/select2.full.js');
$this->registerCssFile('@web/js/select2/dist/css/select2.css');
//$this->registerJsFile('@web/js/page_delete.js');
$this->registerCssFile('@web/css/delete.css');

?>
<p class='main-text-name'><?= Html::encode($this->title) ?></p>

<div class="form-group">
    <div class="form-news">
        <?php
        echo Html::tag('label', Yii::t('app', 'Select News:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('span', Yii::t('app', 'Choose exist news or create new '), ['class' => 'small']);
            echo Html::a(Yii::t('app', ' here'), '/site/add-news', ['style' => 'font-size: 85%']);
            ?>
        </div>
        <?php
        echo Select2::widget([
            'name' => 'news-select',
            'id' => 'news',
            'data' => $data['news-list'],
            'disabled' => $data['news-opt']['disabled'],
            'options' => ['placeholder' => 'Select categories ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 10
            ],
        ]);
        ?>
        <input class="btn btn-success btn-md" id="delete-news" type="button" value="Delete"
               onclick="Delete($('.form-news'))">
    </div>
    <hr>
    <div class="form-category">
        <?php
        echo Html::tag('label', Yii::t('app', 'Select Category:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('span', Yii::t('app', 'Choose exist category or create new '), ['class' => 'small']);
            echo Html::a(Yii::t('app', ' here'), '/site/add-category', ['style' => 'font-size: 85%']);
            ?>
        </div>
        <?php
        echo Select2::widget([
            'name' => 'category-select',
            'id' => 'category',
            'data' => $data['category-list'],
            'disabled' => $data['category-opt']['disabled'],
            'options' => ['placeholder' => 'Select categories ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10
            ],
        ]);
        ?>
        <input class="btn btn-success btn-md" id="delete-categories" type="button" value="Delete"
               onclick="Delete($('.form-category'))">
    </div>
    <hr>
    <div class="form-albums">
        <?php
        echo Html::tag('label', Yii::t('app', 'Select Albums:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('span', Yii::t('app', 'Choose exist albums or create new '), ['class' => 'small']);
            echo Html::a(Yii::t('app', ' here'), '/site/add-album', ['style' => 'font-size: 85%']);
            ?>
        </div>
        <?php
        echo Select2::widget([
            'name' => 'album-select',
            'id' => 'album',
            'data' => $data['album-list'],
            'disabled' => $data['album-opt']['disabled'],
            'maintainOrder' => true,
            'options' => ['placeholder' => 'Select album ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10
            ],
        ]);
        ?>
        <input class="btn btn-success btn-md" id="delete-albums" type="button" value="Delete"
               onclick="Delete($('.form-albums'))">

    </div>
    <hr>
    <div class="form-songs">
        <?php
        echo Html::tag('label', Yii::t('app', 'Select Songs:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('span', Yii::t('app', 'Choose exist songs or create new '), ['class' => 'small']);
            echo Html::a(Yii::t('app', ' here'), '/site/upload-song', ['style' => 'font-size: 85%']);
            ?>
        </div>
        <?php
        echo Select2::widget([
            'name' => 'songs-select',
            'id' => 'songs',
            'data' => $data['songs-list'],
            'disabled' => $data['songs-opt']['disabled'],
            'maintainOrder' => true,
            'options' => ['placeholder' => 'Select songs ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10
            ],
        ]);
        ?>
        <input class="btn btn-success btn-md" id="delete-songs" type="button" value="Delete"
               onclick="Delete($('.form-songs'))">
    </div>
    <hr>
</div>
<div id="infoByButton"></div>
<script>
    function Delete(tag) {
        var data = tag.find('select').val();
        var url = tag.find('input')[1].id;
        $.post("/delete/" + url, {
                data: data
            },
            function (data) {
                data = JSON.parse(data);
                message = tag.find("p.delete-message");
                message.remove();
                if(data.result){
                    tag.append("<p class='alert-success delete-message'>" + data.message + "</p>");
                }else{
                    tag.append("<p class='alert-danger delete-message'>" + data.message + "</p>");
                }
                setTimeout(function () {
                    tag.find("p.delete-message").remove();
                },3000)
            }
        );
    }
</script>