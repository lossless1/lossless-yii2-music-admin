<?php
use yii\helpers\Html;
use kartik\widgets\Select2;

$this->title = "Adding Album";

$this->registerCssFile('@web/css/bootstrap-fileinput-master/css/fileinput.css');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/canvas-to-blob.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/sortable.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/purify.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/fileinput.js');
$this->registerJsFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');

$this->registerCssFile('@web/css/create-pages.css');

?>
<p class='main-text-name'><?= Html::encode($this->title) ?></p>

<div class="form-group">
    <div class="form-album">
        <?php
        echo Html::tag('label', Yii::t('app', 'Album Name:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('span', Yii::t('app', 'You can input new name for album only.'), ['class' => 'small pull-left']);
            ?>
        </div>
        <?php
        echo Html::tag('label', Yii::t('app', 'Max 30 symbols.'), ['class' => 'small pull-right text-success', 'id' => 'remain-sym-album']);
        echo Html::input('text', 'album', '', ['class' => 'form-control', 'id' => 'album']);
        ?>
    </div>
    <div class="form-songs">
            <?php
            echo Html::tag('label', Yii::t('app', 'Select Songs:'), ['class' => 'control-label']);
            ?>
            <div>
                <?php
                echo Html::tag('span', Yii::t('app', 'Choose exist songs or create new '), ['class' => 'small']);
                echo Html::a(Yii::t('app', ' here'), '/site/upload-song', ['style' => 'font-size: 85%']);
                ?>
            </div>
            <?php
            echo Select2::widget([
                'name' => 'songs-select',
                'id' => 'songs',
                'data' => $data['songs-list'],
                'disabled' => $data['songs-opt']['disabled'],
                'maintainOrder' => true,
                'options' => ['placeholder' => 'Select songs ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'maximumInputLength' => 10
                ],
            ]);
            ?>
    </div>
    <div class="form-avatar">
        <?php
        echo Html::tag('label', Yii::t('app', 'Album Avatar:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('span', Yii::t('app', 'Extension for image: img,jpeg,png.'), ['class' => 'small pull-left']);
            ?>
        </div>
        <input class="file-loading" id="avatar" name="avatar-input" type="file" data-show-preview="false">
    </div>
    <div>
        <input class="btn btn-success btn-md" id="send" type="submit">
    </div>

    <div id="info"></div>
</div>
<script>
    $(function () {
        $("#album").keyup(function () {
            var album = $("#album");
            var remain = $("#remain-sym-album");
            remain.html("Max " + (30 - album.val().length) + " symbols");
            if ((30 - album.val().length) < 0) {
                remain.removeClass('text-danger text-success').addClass('text-danger');
            } else {
                remain.removeClass('text-success text-danger').addClass('text-success');
            }
        });

        $("#avatar").fileinput({
            maxFileSize: 2000,
            browseClass: "btn btn-success",
            browseLabel: "Pick Avatar",
            previewFileType: "image",
            allowedFileTypes: ["image"],
            allowedFileExtensions: ["png", "jpeg", "jpg"],

            previewFileIconSettings: {
                'png': '<i class="fa fa-file-photo-o text-warning"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>'
            },
            previewFileExtSettings: {
                'png': function (ext) {
                    return ext.match(/(png|jpeg)$/i);
                },
                'jpg': function (ext) {
                    return ext.match(/(jpg)$/i);
                }
            },
            overwriteInitial: false,
            initialCaption: "Choose avatar...",
            showUpload: false
        });
        $("#send").click(function () {
            var album = $("#album").val();
            var avatar = $("#avatar");
            var songs = JSON.stringify($("#songs").val());

            var form = new FormData();
            $.each(avatar[0].files, function (key, value) {
                form.append("avatar", value);
            });
            console.log(songs);
            form.append("album_name", album);
            form.append("songs", songs);

            $("p.alert-success").remove();
            $("p.alert-danger").remove();

            $.ajax({
                url: "/save/save-album",
                data: form,
                type: "post",
                cache: false,
                contentType: false,
                processData: false
            }).done(function (response) {
                response = JSON.parse(response);
                console.log(response);
                if (response.result) {
                    $(".form-group").append("<p class='alert alert-success'>" + response.content + "</p>");
                    console.log(response.content);
                } else {
                    $(".form-group").append("<p class='alert alert-danger'>" + response.content + "</p>");
                    console.log(response.content);
                }
                setTimeout(
                    function () {
                        $('.alert').fadeOut('fast');
                    }, 3000);
            }).fail(function (error) {
                console.log(error.responseText);
            });
        });
    })
</script>