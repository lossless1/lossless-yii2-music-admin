<?php
use yii\helpers\Html;
$this->title = 'Show news page';
$this->registerJsFile('@web/js/page_show_news');

?>
<div id="infoNews">
    <p class='main-text-name'><?= Html::encode($this->title)?></p>

</div>
