<?php
use yii\helpers\Html;

$this->title = "Adding Category";

$this->registerCssFile('@web/css/bootstrap-fileinput-master/css/fileinput.css');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/canvas-to-blob.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/sortable.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/plugins/purify.js');
$this->registerJsFile('@web/css/bootstrap-fileinput-master/js/fileinput.js');
$this->registerJsFile('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');

$data = [];
?>
<p class='main-text-name'><?= Html::encode($this->title) ?></p>

<div class="form-group">
    <div class="form-category">
        <?php
        echo Html::tag('label', Yii::t('app', 'Category Name:'), ['class' => 'control-label']);
        ?>
        <div>
            <?php
            echo Html::tag('p', Yii::t('app', 'You can input new name for category only.'), ['class' => 'small pull-left']);
            echo Html::tag('label', Yii::t('app', 'Max 30 symbols.'), ['class' => 'small pull-right text-success','id'=>'remain-sym-content']);
            echo Html::input('text', 'category', '', ['class' => 'form-control', 'id' => 'category']);
            ?>
        </div>
    </div>
    <div>
        <input class="btn btn-success btn-md" id="send" type="submit">
    </div>

    <div id="info"></div>
</div>
<script>
    $(function () {
        $("#send").click(function () {
            var author = $("#category").val();

            var form = new FormData();
            form.append("category_name", author);

            $("p.alert-success").remove();
            $("p.alert-danger").remove();
            $.ajax({
                url: "/save/save-category",
                data: form,
                type: "post",
                cache: false,
                contentType: false,
                processData: false
            }).done(function (response) {
                response = JSON.parse(response);
                console.log(response);
                if (response.status) {
                    $(".form-group").append("<p class='alert alert-success'>" + response.content + "</p>");
                    console.log(response.content);
                } else {
                    $(".form-group").append("<p class='alert alert-danger'>" + response.content + "</p>");
                    console.log(response.content);
                }
                setTimeout(
                    function () {
                        $('.alert').fadeOut('fast');
                    }, 3000);
            }).fail(function (error) {
                $(".form-group").append("<p class='alert alert-danger'>" + error.responseText + "</p>");
                console.log(error.responseText);
            });


        });
    })

    $("#category").keyup(function(){
        var remain = $("#remain-sym-content");
        remain.html("Max "+ (30 - $("#category").val().length) + " symbols");
        if((30 - $("#category").val().length)<0){
            remain.removeClass('text-danger text-success').addClass('text-danger');
        }else{
            remain.removeClass('text-success text-danger').addClass('text-success');
        }
    });
</script>