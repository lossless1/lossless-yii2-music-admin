<?php
use yii\helpers\Html;
use kartik\switchinput\SwitchInput;

$this->title = "Search songs";
$this->registerCssFile('@web/css/search.css');
?>
<p class='main-text-name'><?= Html::encode($this->title) ?></p>

<hr>

<form class='input-group inpusearch-song-inp'>
    <input class="form-control" id="name" type=text value="" size="20" autocomplete='off'/>

    <div class='input-group-btn'>
        <input class="btn btn-success" onclick="sendMusic()" value='Search'>
    </div>
</form>
<form>
    <input name="check" id="check_authors" type="radio" value="author">
    <label for="check_authors">by authors</label>
    <input name="check" id="check_songs" type="radio" value="song" checked>
    <label for="check_songs">by songs</label>
    <input name="check" id="check_albums" type="radio" value="album">
    <label for="check_albums">by albums</label>

</form>
<div class="songs"></div>
<script>
    function sendMusic() {
        var name = $('#name').val();
        var search = $("input[name=check]:checked").val();
        $.ajax({
            url: "/search/search-by-"+search,
            type: "POST",
            data: {
                name: name
            },
            error: function (error) {
                console.log(error);
            },
            success: function (msg) {
                json = JSON.parse(msg);
                console.log(json);
                $(".songs").empty();
                $.each(json, function (key, value) {
                    CreateTag(value);
                    console.log(value);
                });
            }
        });
    }

    function CreateTag(data) {
        var tagSong, nameObject, audioObject;

        tagSong = $(".songs");

        tagSong.append(BuildObject(data));
    }

    function BuildObject(data) {
        var tag = $("<div>");
        tag.attr("id","song_"+data.id_song);
        tag.attr("class","song");

        nameObject = $("<a>");
        nameObject.attr("href", "/site/song");//.php?id=" + data.id + "&fullname=" + data.fullname);/////
        nameObject.attr("class", "song-link");
        nameObject.html(data.song_name);
        tag.append(nameObject);

        audioObject = BuildAudioObject(data);
        tag.append(audioObject);

        return tag;
    }

    function BuildAudioObject(data) {
        var audioObject;
        var sourceObject;
        var path = '/songs/' + data.filename;
        audioObject = $("<audio>");
        audioObject.attr("controls", "");
        audioObject.attr("style", "padding:0");
        audioObject.addClass("col-xs-12");
        sourceObject = $("<source>");
        sourceObject.attr("src", path);
        sourceObject.attr("type", "audio/ogg");
        audioObject.append(sourceObject);
        return audioObject;
    }
</script>