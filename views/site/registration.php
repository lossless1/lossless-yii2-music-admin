<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->registerJsFile('@web/js/reglog.js');
$this->title = "Registration";
?>
<div id="divRegister">
    <p class='main-text-name'><?= Html::encode($this->title)?></p>

    <div class='form-group-sm col-lg-10'>
        <?php $form = ActiveForm::begin([
            'id'=>'registration-form',
            'options' => [
                'class' => 'col-lg-10',
            ],

            'layout' => 'horizontal',
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-lg-4',
                ],
                'template' => "<div class='col-lg-5'>{label}</div><div class='col-lg-8'>{input}</div>\n<div class='col-lg-8'>{error}</div>",
                'labelOptions' => ['class'=>'col-lg-12 control-panel'],

            ]
        ])?>

        <?= $form->field($model,'username')->textInput(['autofocus'=>true]);?>
        <?= $form->field($model,'password')->passwordInput();?>
        <?= $form->field($model,'re_password')->passwordInput();?>
        <?= $form->field($model,'email')->textInput();?>
        <?= Html::submitButton('Submit',['id'=>'Register','class'=>'btn btn-primary col-lg-3','action'=>'/site/search']);?>
        <?php ActiveForm::end();?>
    </div>
</div>
