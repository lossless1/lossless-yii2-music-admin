<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "core_avatars".
 *
 * @property int $id_avatar
 * @property string $avatar_name
 * @property string $avatar_fullname
 * @property string $avatar_extension
 * @property string $avatar_path
 * @property string $avatar_crypt
 * @property string $avatar_base64
 * @property int $avatar_size
 * @property string $created
 */
class CoreAvatars extends \yii\db\ActiveRecord
{
    public $_data = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{core_avatars}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['avatar_size'], 'integer'],
            [['avatar_base64'], 'string'],
            [['created'], 'safe'],
            [['avatar_name', 'avatar_fullname', 'avatar_extension', 'avatar_path', 'avatar_crypt'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_avatar' => Yii::t('app', 'Id Avatar'),
            'avatar_name' => Yii::t('app', 'Avatar Name'),
            'avatar_fullname' => Yii::t('app', 'Avatar Fullname'),
            'avatar_extension' => Yii::t('app', 'Avatar Extension'),
            'avatar_path' => Yii::t('app', 'Avatar Path'),
            'avatar_crypt' => Yii::t('app', 'Avatar Crypt'),
            'avatar_base64' => Yii::t('app', 'Avatar Base64'),
            'avatar_size' => Yii::t('app', 'Avatar Size'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    public function inputData($data,$path)
    { //$_FILES['avatar']
        $this->_data = $data;
        $this->_data['avatar_path'] = Yii::getAlias("@app/web/images/avatars/".$path."/" . $this->_data['name']);
    }

    public function uploadAvatar()
    {
        $response = [];
        //news $_FILES['avatar'] author
        if (!empty($this->_data)) {
            $avatarBase64 = $this->getAvatarBase64($this->_data['tmp_name']); //need inputData()
            $findAvatar = CoreAvatars::findOne(['avatar_base64' => $avatarBase64]);
            if(!$findAvatar){
                move_uploaded_file($this->_data['tmp_name'], $this->_data['avatar_path']);
                $this->inputAvatar();
                $response['result'] = true;
                $response['content'] = Json::encode("Success creating author.");
                return $response;
            }else{
                $response['result'] = false;
                $response['content'] = Json::encode("Duplicating avatar.");
                return $response;
            }
        } else {
            $response['result'] = false;
            $response['content'] = Json::encode("Input Avatar.");
            return $response;
        }
    }

    public function uploadDefaultAvatar()
    {
        $this->_data['name'] = 'default.jpg';
        $this->_data['avatar_path'] = Yii::getAlias("@app/web/images/avatars/default/author/default.jpg");
        //Checking on dublicate default avatar
        $findAvatar = CoreAvatars::findOne(['avatar_name'=>'default']);
        if(!$findAvatar){
            $this->inputDefaultAvatar();
        }
    }

    public function inputDefaultAvatar()
    {
        $modelAvatar = new CoreAvatars();
        $modelAvatar->avatar_name = "default";
        $modelAvatar->avatar_fullname = "default";
        $modelAvatar->avatar_extension = null;
        $modelAvatar->avatar_path = $this->_data['avatar_path'];
        $modelAvatar->avatar_size = 0;//WOULD BE UPDATED to default
        $modelAvatar->avatar_base64 = $this->getAvatarBase64();
        if ($modelAvatar->save()) {
            return true;
        } else {
            return false;
        }
    }

    public function inputAvatar()
    {
        $modelAvatar = new CoreAvatars();
        $modelAvatar->avatar_name = $this->getAvatarName();
        $modelAvatar->avatar_fullname = $this->_data['name'];
        $modelAvatar->avatar_extension = $this->getAvatarExtension();;
        $modelAvatar->avatar_path = $this->_data['avatar_path'];
        $modelAvatar->avatar_crypt = "";//NEED TO CRYPT
        $modelAvatar->avatar_size = intval($this->_data['size']);//WOULD BE UPDATED
        $modelAvatar->avatar_base64 = $this->getAvatarBase64();
        if ($modelAvatar->save()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAvatarName()
    {
        $arr = explode(".", $this->_data['name']);
        $countArr = count($arr);
        unset($arr[$countArr - 1]);
        $avatar = implode($arr);
        $this->_data['avatar_short_name'] = $avatar;
        return $this->_data['avatar_short_name'];
    }

    public function getAvatarExtension()
    {
        $arr = explode(".", $this->_data['name']);
        $countArr = count($arr);
        $this->_data['avatar_extension'] = $arr[$countArr - 1];
        return $this->_data['avatar_extension'];
    }

    public function getAvatarBase64($path = false)
    {
        if(!empty($path)){
            $base64 = base64_encode(file_get_contents($path));
        }else{
            $base64 = base64_encode(file_get_contents($this->_data['avatar_path']));
        }
        return "data:image/" . $this->getAvatarExtension() . ";base64," . $base64;
    }


    public static function createAndGetIdAvatar($data,$path){
        $modelAvatar = new CoreAvatars();
        $modelAvatar->inputData($data,$path);
        $avatarBase64 = $modelAvatar->getAvatarBase64($data['tmp_name']);
        $findAvatar = CoreAvatars::findOne(['avatar_base64' => $avatarBase64]);
        if(!$findAvatar) {
            $modelAvatar->uploadAvatar();
            $findAvatar = CoreAvatars::findOne(['avatar_base64' => $avatarBase64]);
        }
        return $findAvatar->id_avatar;
    }
    public static function createAndGetIdDefaultAvatar() //not required parameters
    {
        $modelAvatar = new CoreAvatars();
        $modelAvatar->uploadDefaultAvatar();
        $findAvatar = CoreAvatars::findOne(['avatar_name' => 'default']);
        $idAvatar =  $findAvatar->id_avatar;
        return $idAvatar;
    }
}