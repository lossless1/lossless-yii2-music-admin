<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "core_tags".
 *
 * @property string $id_tag
 * @property string $name_tag
 * @property string $created
 */
class CoreTags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created'], 'safe'],
            [['name_tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tag' => Yii::t('app', 'Id Tag'),
            'name_tag' => Yii::t('app', 'Name Tag'),
            'created' => Yii::t('app', 'Created'),
        ];
    }
    public static function createTags($data){
        $modeltags = new CoreTags();
        $modeltags->name_tag = isset($data['name_tag'])?$data['name_tag']:null;
        if($modeltags->save()){
            return $modeltags->id_tag;
        }
        return false;
    }
}
