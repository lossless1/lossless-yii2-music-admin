<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "core_songs".
 *
 * @property int $id_song
 * @property int $id_author
 * @property int $id_album
 * @property string $filename
 * @property string $song_name
 * @property string $original_name
 * @property string $description
 * @property string $crypt
 * @property string $chrono
 * @property int $likes
 * @property string $created
 */
class CoreSongs extends \yii\db\ActiveRecord
{
    private $authorName;
    private $songName;
    private $originalName;
    private $md5data;
    private $fullName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_songs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_author', 'id_album', 'likes'], 'integer'],
            [['created'], 'safe'],
            [['filename', 'song_name', 'original_name', 'description', 'crypt', 'chrono'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_song' => Yii::t('app', 'Id Song'),
            'id_author' => Yii::t('app', 'Id Author'),
            'id_album' => Yii::t('app', 'Id Album'),
            'filename' => Yii::t('app', 'Filename'),
            'song_name' => Yii::t('app', 'Song Name'),
            'original_name' => Yii::t('app', 'Original Name'),
            'description' => Yii::t('app', 'Description'),
            'crypt' => Yii::t('app', 'Crypt'),
            'chrono' => Yii::t('app', 'Chrono'),
            'likes' => Yii::t('app', 'Likes'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    public function getAuthor()
    {
        return $this->hasMany(CoreAuthors::className(), ['id_author' => 'id_author']);
    }

    public function getAlbums()
    {
        return $this->hasMany(CoreAlbums::className(), ['id_album' => 'id_album']);
    }

    public function uploadSongs($data)
    {
        $response = [];
        if ($data) {
            foreach ($data as $key => $value) {

                $this->fullName = $this->songFullName($key);
                $this->originalName = preg_replace("/.mp3|.mp4|.mpeg/i", "", $this->fullName);
                $this->authorName = $this->authorInSong($this->originalName);
                $this->songName = $this->songName($this->originalName);
                $this->md5data = md5_file($_FILES[$key]['tmp_name']);

                $cryptFile = $this->findOne(['crypt' => $this->md5data]);

                if ($cryptFile && $this->md5data == $cryptFile['md5']) {
                    $response['result'] = false;
                    $response['content'] = $this->authorName . " - " . $this->songName . Yii::t('app', ": song is already there! Download something else!");
                } else {
                    $this->createFieldSong();
                    $outputInfo = " Here is some more debugging info: $this->authorName - $this->songName";
                    if (move_uploaded_file($_FILES[$key]['tmp_name'], Yii::getAlias('@app/web/songs/') . $this->fullName)) {
                        $response['result'] = true;
                        $response['content'] = Yii::t('app', "UPLOAD_SUCCESS") . $outputInfo;
                    } else {
                        $response['result'] = false;
                        $response['content'] = Yii::t('app', "UPLOAD_FAIL") . $outputInfo;
                    }
                }
            }
        }

        return $response;
    }

    public function createFieldSong()
    {
        $db = new CoreSongs();
        $db->id_author = 0;
        $db->id_album = 0;
        $db->filename = strval($this->fullName);
        $db->song_name = strval($this->songName);
        $db->original_name = strval($this->originalName);
        $db->description = strval(uniqid());
        $db->crypt = strval($this->md5data);
        //$db->chrono = $this->MP3(Yii::getAlias('@app/web/songs/') . $this->fullName); // NOT WORKING
        $db->likes = 0;
        $db->save();
    }

    public function songName($orgname)
    {
        $song = preg_replace("/^\w+\-/i", " ", $orgname);
        $song = preg_replace("/\_/", " ", $song);
        $song = preg_replace("/^\s+/", "", $song);
        $song = mb_convert_case($song, MB_CASE_TITLE, "UTF-8");
        return $song;
    }

    public function authorInSong($orgname)
    {
        $author = preg_replace("/\-\w+$/i", " ", $orgname);
        $author = preg_replace("/\_/", " ", $author);
        $author = preg_replace("/\s+$/", "", $author);
        $author = mb_convert_case($author, MB_CASE_TITLE, "UTF-8");
        return $author;
    }

    public function songFullName($key)
    {
        $fileNameWithoutMinus = preg_replace("/\–/i", "-", $_FILES[$key]['name']);
        $lowFileNameWithoutMinus = strtolower($fileNameWithoutMinus);
        $fileNameWithoutLessWord = preg_replace("/(\()|(\))|(_\(zaycev.net\))/i", "", $lowFileNameWithoutMinus);
        $fullName = preg_replace("/(\s)/i", "_", $fileNameWithoutLessWord);
        $this->fullName = $fullName;
        return $fullName;
    }

    public function MP3()
    {
        $chrono = new \MP3File('../web/songs/arctic_monkeys.mp3');
        $duration = $chrono->getDuration();
        $duration = $chrono->formatTime($duration);
        return $duration;
    }
}
