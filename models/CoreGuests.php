<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "core_guests".
 *
 * @property int $id_guest
 * @property int $ip_guest
 * @property string $created
 */
class CoreGuests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_guests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip_guest'], 'integer'],
            [['created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_guest' => Yii::t('app', 'Id Guest'),
            'ip_guest' => Yii::t('app', 'Ip Guest'),
            'created' => Yii::t('app', 'Created'),
        ];
    }
}
