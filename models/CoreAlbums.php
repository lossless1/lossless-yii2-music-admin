<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "core_albums".
 *
 * @property int $id_album
 * @property int $id_author
 * @property int $id_avatar
 * @property string $id_songs
 * @property string $name_album
 * @property string $created
 */
class CoreAlbums extends \yii\db\ActiveRecord
{
    public $_data = [];
    public $_songs = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_albums';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_avatar','id_author'], 'integer'],
            [['created'], 'safe'],
            [['name_album', 'id_songs'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_album' => Yii::t('app', 'Id Album'),
            'id_author' => Yii::t('app', 'Id Author'),
            'id_songs' => Yii::t('app', 'Id Songs'),
            'id_avatar' => Yii::t('app', 'Id Avatar'),
            'name_album' => Yii::t('app', 'Name Album'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(CoreAuthors::className(), ['id_author' => 'id_author']);
    }

    public function getAvatar()
    {
        return $this->hasOne(CoreAvatars::className(), ['id_avatar' => 'id_avatar']);
    }

    public function createAlbum($post)
    {
        $response['result'] = false;
        if (isset($post)) {
            if ($this->checkRequireKey($post)) {
                $this->_data = $post;
                $data['avatar'] = !empty($_FILES['avatar']) ? $_FILES['avatar'] : '';
                $this->_songs = json_decode($post['songs']);
                $albumDb = new CoreAlbums();
                $albumDb->id_avatar = isset($_FILES['avatar']) ?
                    CoreAvatars::createAndGetIdAvatar($data['avatar'], 'albums') :
                    CoreAvatars::createAndGetIdDefaultAvatar();
                $albumDb->id_songs = isset($post['songs']) ? $this->inputSongs() : '';
                $albumDb->id_author = 0;
                $albumDb->name_album = isset($post['album_name']) ? ucwords($post['album_name']) : '';
                if ($albumDb->save()) {
                    $this->changeAlbumInSongs($albumDb->id_album);
                    $response['result'] = true;
                    $response['content'] = "Success creating album.";
                    return $response;
                } else {
                    $response['content'] = "Failed creating album";
                    return $response;
                }
            }
            $response['content'] = "Not required key";
            return $response;
        }
    }

    public function checkRequireKey($post)
    {
        if (empty($post['album_name']) || empty($post['songs'])) {
            return false;
        } else {
            return true;
        }
    }

    public function inputSongs()
    {
        if ($this->_songs) {
            $response = [];
            foreach ($this->_songs as $song) {
                $modelSong = CoreSongs::findOne(['song_name' => $song]);
                if ($modelSong) {
                    $response[] = $modelSong->id_song;
                } else {
                    $response[] = null;
                }
            }
            return Json::encode($response);
        }
        return false;
    }

    public function changeAlbumInSongs($lastId)
    {
        if ($this->_songs) {
            foreach ($this->_songs as $song) {
                $modelSong = CoreSongs::findOne(['song_name' => $song]);
                if ($modelSong) {
                    $modelSong->id_album = $lastId;
                    $modelSong->save();
                }
            }
        }
    }
}
