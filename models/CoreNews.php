<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "core_news".
 *
 * @property int $id_news
 * @property int $id_category
 * @property int $id_author
 * @property string $id_tags
 * @property string $id_songs
 * @property int $id_avatar
 * @property int $likes
 * @property string $title
 * @property string $content
 * @property string $created
 */
class CoreNews extends \yii\db\ActiveRecord
{
    private $_category;
    private $_author;
    private $_tags;
    private $_songs;
    private $_avatar;
    private $_title;
    private $_content;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_author', 'likes', 'id_avatar', 'id_category'], 'integer'],
            [['created'], 'safe'],
            [['title', 'content', 'id_tags', 'id_songs'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_news' => Yii::t('app', 'Id News'),
            'id_category' => Yii::t('app', 'Id Category'),
            'id_author' => Yii::t('app', 'Id Author'),
            'id_tags' => Yii::t('app', 'Id Tags'),
            'id_songs' => Yii::t('app', 'Id Song'),
            'id_avatar' => Yii::t('app', 'Id Avatar'),
            'likes' => Yii::t('app', 'Likes'),
            'link_song' => Yii::t('app', 'Link Song'),
            'link_picture' => Yii::t('app', 'Link Picture'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    public function uploadNews($post)
    {
        $response = [];
        if ($this->getValidateNewsForms($post)) {
            $this->_category = $post['category'];
            $this->_author = $post['author'];
            $this->_tags = json_decode($post['tags']);
            $this->_songs = isset($post['songs']) ? json_decode($post['songs']) : "";
            $this->_avatar = $_FILES['avatar'];
            $this->_title = $post['title'];
            $this->_content = isset($post['content']) ? $post['content'] : "";
            $this->inputNews();

            $response['result'] = true;
            $response['content'] = Yii::t('app', "News created");
            return $response;
        } else {
            $response['result'] = false;
            $response['content'] = Yii::t('app', "Not all fields is fill");
            return $response;
        }
    }

    public function getValidateNewsForms($data)
    {
        return !empty($data['author']) && !empty($data['category']) && !empty($data['tags'])
        && !empty($data['title']) && isset($_FILES['avatar']);
    }

    public function inputNews()
    {
        $modelnews = new CoreNews();
        $modelnews->id_category = $this->inputCategory();//NEED SEARCH
        $modelnews->id_author = $this->inputAuthor();//NEED SEARCH
        $modelnews->id_tags = $this->inputTags();//NEED JSON SONGS
        $modelnews->id_songs = $this->inputSongs();
        $modelnews->id_avatar = $this->inputAvatar();//NEED SEARCH
        $modelnews->likes = 0;//QUANTITY LIKES
        $modelnews->title = $this->_title;//QUANTITY LIKES
        $modelnews->content = $this->_content;
        $modelnews->save();
    }

    public function inputCategory()
    {
        if ($this->_category) {
            $modelCategory = CoreCategory::findOne(['category_name' => $this->_category]);
            if (!$modelCategory) {
                $data['category_name'] = $this->_category;
                CoreCategory::createCategory($data);
                $modelCategory = CoreCategory::findOne(['category_name' => $this->_category]);
            }
            return $modelCategory->id_category;
        }
        return false;
    }

    public function inputAuthor()
    {
        if ($this->_author) {
            $modelAuthor = CoreAuthors::findOne(['author_name' => $this->_author]);
            if (!$modelAuthor) {
                $data['author_name'] = $this->_author;
                CoreAuthors::createAuthor($data);
                $modelAuthor = CoreAuthors::findOne(['author_name' => $this->_author]);
            }
            return $modelAuthor->id_author;
        }
        return false;
    }

    public function inputTags()
    {
        if ($this->_tags) {
            $response = [];
            foreach ($this->_tags as $tag) {
                $modelTag = CoreTags::findOne(['name_tag' => $tag]);
                if ($modelTag) {
                    $response[] = $modelTag->id_tag;
                } else {
                    $data['name_tag'] = $tag;
                    $response[] = CoreTags::createTags($data);
                }
            }
            return Json::encode($response);
        }
        return false;
    }

    public function inputAvatar()
    {
        if ($this->_avatar) {
            $avatar = new CoreAvatars();
            $avatar->inputData($this->_avatar);
            $avatar->uploadAvatar($this->_avatar);
            $modelAuthor = CoreAvatars::findOne(['avatar_fullname' => $this->_avatar['name']]); //TODO SEARCH ON CRYPT
            return Json::encode($modelAuthor->id_avatar);
        }
        return false;
    }

    public function inputSongs()
    {
        if ($this->_songs) {
            $response = [];
            foreach ($this->_songs as $song) {
                $modelSong = CoreSongs::findOne(['song_name' => $song]);
                if ($modelSong) {
                    $response[] = $modelSong->id_song;
                } else {
                    $response[] = null;
                }
            }
            return Json::encode($response);
        }
        return "";
    }
}
