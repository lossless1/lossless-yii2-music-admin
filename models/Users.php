<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id_user
 * @property string $user_login
 * @property string $user_password
 * @property string $user_email
 * @property string $id_news_liked
 * @property string $id_news_added
 * @property string $avatar_path
 * @property string $id_tags
 * @property int $id_user_vk
 * @property int $id_user_google
 * @property int $id_user_facebook
 * @property int $id_user_twitter
 * @property string $created
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $username;
    public $password;
    public $re_password;
    public $email;

    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_vk', 'id_user_google', 'id_user_facebook', 'id_user_twitter'], 'integer'],
            [['created'], 'safe'],
            [['user_login', 'user_password', 'user_email', 'id_news_liked', 'id_news_added', 'avatar_path', 'id_tags'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => Yii::t('app', 'ID'),
            'user_login' => Yii::t('app', 'User Login'),
            'user_password' => Yii::t('app', 'User Password'),
            'user_email' => Yii::t('app', 'User Email'),
            'id_news_liked' => Yii::t('app', 'Id News Liked'),
            'id_news_added' => Yii::t('app', 'Id News Added'),
            'avatar_path' => Yii::t('app', 'Avatar Path'),
            'id_tags' => Yii::t('app', 'Id Tags'),
            'id_user_vk' => Yii::t('app', 'Id User Vk'),
            'id_user_google' => Yii::t('app', 'Id User Google'),
            'id_user_facebook' => Yii::t('app', 'Id User Facebook'),
            'id_user_twitter' => Yii::t('app', 'Id User Twitter'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    public static function getUserById($id){
        return static::findOne(['id_user' => $id]);
    }

    public function registerNewUser($data){
        $db = new Users();
        $db->user_login = $data['username'];
        $db->user_password = $data['password'];
        $db->user_email = $data['email'];
        $db->save();
    }

    public function loginUser($data){
        $user = self::findOne(['username'=>$data['username'],'password'=>$data['password']]);
        if($user){
            Yii::$app->user->login($data['username']);
            return true;
        }
        return false;
    }
}
