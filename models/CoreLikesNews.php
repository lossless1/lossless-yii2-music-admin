<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "core_likes_news".
 *
 * @property int $id_news
 * @property int $id_user
 * @property string $created
 */
class CoreLikesNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_likes_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
            [['created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_news' => Yii::t('app', 'Id News'),
            'id_user' => Yii::t('app', 'Id User'),
            'created' => Yii::t('app', 'Created'),
        ];
    }
}
