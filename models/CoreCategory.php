<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "core_category".
 *
 * @property int $id_category
 * @property string $category_name
 * @property string $created
 */
class CoreCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created'], 'safe'],
            [['category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_category' => Yii::t('app', 'Id Category'),
            'category_name' => Yii::t('app', 'Category Name'),
            'created' => Yii::t('app', 'Created'),
        ];
    }
    public static function createCategory($data){
        $response =[
            'status' => false,
            'content' => '',
        ];
        if(isset($data['category_name'])&&$data['category_name'] < 30){
            $modelCategory = new CoreCategory();
            $modelCategory->category_name = isset($data['category_name'])?$data['category_name']:null;;
            if($modelCategory->save()){
                $response['status'] = true;
                $response['content'] = 'Category saved';
            }else{
                $response['content'] = 'Category not saved';
            }
        }else{
            $response['content'] = 'Text to long';
        }
        return $response;
    }

}
