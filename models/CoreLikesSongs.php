<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "core_likes_songs".
 *
 * @property int $id_song
 * @property int $id_user
 * @property string $created
 */
class CoreLikesSongs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_likes_songs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'id_user'], 'integer'],
            [['created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_song' => Yii::t('app', 'Id Song'),
            'id_user' => Yii::t('app', 'Id User'),
            'created' => Yii::t('app', 'Created'),
        ];
    }
}
