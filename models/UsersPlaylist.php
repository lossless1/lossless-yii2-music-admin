<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_playlist".
 *
 * @property int $id_user
 * @property int $id_news
 * @property string $created
 */
class UsersPlaylist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_playlist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_news'], 'integer'],
            [['created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => Yii::t('app', 'Id User'),
            'id_news' => Yii::t('app', 'Id News'),
            'created' => Yii::t('app', 'Created'),
        ];
    }
}
