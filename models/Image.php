<?php
/**
 * Created by PhpStorm.
 * User: lossless
 * Date: 2/11/17
 * Time: 01:19
 */

namespace app\models;
use yii\web\UploadedFile;


class Image extends \yii\db\ActiveRecord
{
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
}