<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "core_authors".
 *
 * @property int $id_author
 * @property string $author_name
 * @property string $id_albums ARRAY ALBUMS
 * @property int $id_avatar
 * @property int $quantity_subs
 * @property string $created
 */
class CoreAuthors extends \yii\db\ActiveRecord
{
    private $_albums;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_avatar', 'quantity_subs'], 'integer'],
            [['created'], 'safe'],
            [['author_name', 'id_albums'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_author' => Yii::t('app', 'Id Author'),
            'author_name' => Yii::t('app', 'Author Name'),
            'id_albums' => Yii::t('app', 'Id Albums'),
            'id_avatar' => Yii::t('app', 'Id Avatar'),
            'quantity_subs' => Yii::t('app', 'Quantity Subs'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    public function getSongs()
    {
        return $this->hasMany(CoreSongs::className(), ['id_author' => 'id_author']);
    }

    public function getAlbums()
    {
        return $this->hasMany(CoreAlbums::className(), ['id_album' => 'id_albums']);
    }

    public function createAuthor($data)
    {
        $response = [];

        if (isset($data)) {
            //CREATING ON AUTHOR PAGE
            $findAuthor = CoreAuthors::findOne(['author_name' => $data['author_name']]);
            if (!$findAuthor) {
                $data['avatar'] = !empty($_FILES['avatar']) ? $_FILES['avatar'] : null;
                $this->_albums = isset($data['albums_name']) ? json_decode($data['albums_name']) : "";
                $modelauthor = new CoreAuthors();
                $modelauthor->author_name = isset($data['author_name']) ?
                    $modelauthor->getAuthorName($data['author_name']) :
                    null;
                $modelauthor->id_albums = isset($data['albums_name']) ?
                    $this->inputAlbums() :
                    null;
                $modelauthor->id_avatar = !empty($data['avatar']) ?
                    CoreAvatars::createAndGetIdAvatar($data['avatar'], 'authors') : //if news avatar | author_name
                    CoreAvatars::createAndGetIdDefaultAvatar(); //if news author avatar  | author_name avatar
                $modelauthor->quantity_subs = 0;

                if ($modelauthor->save()) {
                    $this->changeAuthorInSongs($modelauthor->id_author);
                    $this->changeAuthorInAlbums($modelauthor->id_author);
                    $response['result'] = true;
                    $response['content'] = "Success create author " . $data['author_name'];
                    return $response;
                } else {
                    $response['result'] = false;
                    $response['content'] = "Failed create author " . $data['author_name'];
                    return $response;
                }
            } else {
                $response['result'] = false;
                $response['content'] = "Dublicating author " . $data['author_name'];
                return $response;
            }
        }
        return false;
    }

    public function inputAlbums()
    {
        if ($this->_albums) {
            $response = [];
            foreach ($this->_albums as $albums) {
                $modelAlbum = CoreAlbums::findOne(['name_album' => $albums]);
                if ($modelAlbum) {
                    $response[] = $modelAlbum->id_album;
                } else {
                    $response[] = null;
                }
            }
            return Json::encode($response);
        }
        return false;
    }

    public function changeAuthorInSongs($lastId)
    {
        if ($this->_albums) {
            foreach ($this->_albums as $albums) {
                $modelAlbum = CoreAlbums::findOne(['name_album' => $albums]);
                Yii::$app->db->createCommand()
                    ->update('core_songs', ['id_author' => $lastId], 'id_album = '.$modelAlbum->id_album)
                    ->execute();
            }
        }
    }

    public function changeAuthorInAlbums($lastId)
    {
        if ($this->_albums) {
            foreach ($this->_albums as $albums) {
                $modelAlbum = CoreAlbums::findOne(['name_album' => $albums]);
                Yii::$app->db->createCommand()
                    ->update('core_albums', ['id_author' => $lastId], 'id_album = '.$modelAlbum->id_album)
                    ->execute();
            }
        }
    }

    public function getAuthorName($orgname)
    {
        $author = preg_replace("/\-\w+$/i", " ", $orgname);
        $author = preg_replace("/\_/", " ", $author);
        $author = preg_replace("/\s+$/", "", $author);
        $author = mb_convert_case($author, MB_CASE_TITLE, "UTF-8");
        return $author;
    }

    public function getAuthorFullPath($name)
    {
        $author = Yii::getAlias("@app/web/images/avatars/authors/" . $name);
        return $author;
    }
}

