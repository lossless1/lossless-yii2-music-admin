<?php
namespace app\controllers;

use app\models\{CoreAlbums,CoreAuthors,CoreSongs};
use app\models\CoreAvatars;
use app\models\CoreCategory;
use app\models\CoreLikesNews;
use app\models\CoreNews;
use Yii;
use yii\web\Controller ;

class SearchController extends Controller {

    public function actionSearchByAuthor(){
        $post = Yii::$app->request->post();

        $data = CoreAuthors::find()->with('songs','albums')->asArray()->all();

        return json_encode($data);
    }

    public function actionSearchBySong(){
        $post = Yii::$app->request->post();

        $data = CoreSongs::find()->where(['like','original_name',$post['name']])->with('author','albums')->asArray()->all();

        return json_encode($data);
    }

    public function actionSearchByAlbum(){
        $post = Yii::$app->request->post();

        $data = CoreSongs::find()->with('author','songs')->asArray()->all();

        return json_encode($data);
    }
}
?>

