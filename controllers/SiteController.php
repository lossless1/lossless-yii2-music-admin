<?php

namespace app\controllers;

use app\models\CoreAlbums;
use app\models\CoreAuthors;
use app\models\CoreCategory;
use app\models\CoreNews;
use app\models\CoreSongs;
use app\models\CoreTags;
use app\models\Image;
use app\models\Users;
use dektrium\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'signup', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup', 'logout'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionIndex()
    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->render('index');
//        }

        $model = new User();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->render('index');
    }

    public function actionSearch()
    {
        $song = '';
        return $this->render('search', ['song' => $song]);
    }

    public function actionDebug()
    {
        return $this->render('debug');

    }

    /**
     * @return string
     */
    public function actionRegistration()
    {

        $model = new Users();
        if ((Yii::$app->request->post())) {
            return $this->render('index');
        }

        return $this->render('registration', ['model' => $model]);
    }

    public function actionNews()
    {
        return $this->render('news');
    }

    public function actionOptions()
    {
        return $this->render('options');
    }

    public function actionAddNews()
    {
        $data = $this->findData();

        return $this->render('addNews', ['data' => $data]);
    }

    public function actionAddAuthor()
    {
        $data = $this->findData();

        return $this->render('addAuthor', ['data' => $data]);
    }

    public function actionAddCategory()
    {
        return $this->render('addCategory');
    }

    public function actionAddAlbum()
    {
        $data = [];
        $songsDb = CoreSongs::findAll(['id_album'=>0]);
        if ($songsDb) {
            foreach ($songsDb as $value) {
                $data['songs-list'][$value->song_name] = $value->song_name;
                $data['songs-opt']['disabled'] = false;
            }
        } else {
            $data['songs-list'] = null;
            $data['songs-opt']['disabled'] = true;
        }
        return $this->render('addAlbum', ['data' => $data]);
    }


    public function actionDelete()
    {
        $data = $this->findData();

        return $this->render('delete',['data'=>$data]);
    }

    public function findData(){
        $data = [];
        $categoryDb = CoreCategory::find()->all();
        if ($categoryDb) {
            foreach ($categoryDb as $value) {
                $data['category-list'][$value->category_name] = $value->category_name;
                $data['category-opt']['disabled'] = false;
            }
        } else {
            $data['category-list'] = null;
            $data['category-opt']['disabled'] = true;
        }
        $newsDb = CoreNews::find()->all();
        if ($newsDb) {
            foreach ($newsDb as $value) {
                $data['news-list'][$value->title] = $value->title;
                $data['news-opt']['disabled'] = false;
            }
        } else {
            $data['news-list'] = null;
            $data['news-opt']['disabled'] = true;
        }
        $albumsDb = CoreAlbums::find()->all();
        if ($albumsDb) {
            foreach ($albumsDb as $value) {
                $data['album-list'][$value->name_album] = $value->name_album;
                $data['album-opt']['disabled'] = false;
            }
        } else {
            $data['album-list'] = null;
            $data['album-opt']['disabled'] = true;
        }
        $songsDb = CoreSongs::find()->all();
        if ($songsDb) {
            foreach ($songsDb as $value) {
                $data['songs-list'][$value->song_name] = $value->song_name;
                $data['songs-opt']['disabled'] = false;
            }
        } else {
            $data['songs-list'] = null;
            $data['songs-opt']['disabled'] = true;
        }
        $tagsDb = CoreTags::find()->all();
        if ($tagsDb) {
            foreach ($tagsDb as $value) {
                $data['tags-list'][$value->name_tag] = $value->name_tag;
            }
        } else {
            $data['tags-list'] = null;
        }
        return $data;
    }

    public function actionUserCabinet()
    {
        return $this->render('userCabinet');
    }

    public function actionUploadSong()
    {

        $post = Yii::$app->request->post();
        return $this->render('uploadSong', ['post' => $post]);
    }
}
