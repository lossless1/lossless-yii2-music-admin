var tagDiv,optionsTag;
var url = APP_URL + "/controllers/dispatcher.php?command=";

$(document).ready(function () {

    CreateOption();
    CreateLogOut();
});
function CreateOption(){
    tagDiv = $("#cabinet");
    optionsTag = $("<a>");
    optionsTag.attr("href",APP_URL+"/views/options.php");
    optionsTag.html("Настройки пользователя");
    tagDiv.append(optionsTag);
}
function CreateLogOut(){
    tdTag = $("<td>");
    pTag = $("<p>");
    aTag = $("<a>");
    pTag.attr("align","right");
    aTag.attr("href", url+"logout");
    aTag.html("Выйти из личного кабинета");
    pTag.append(aTag);
    tdTag.append(pTag);
    $("tr").append(tdTag);
}