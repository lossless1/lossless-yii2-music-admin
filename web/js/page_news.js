var aTag, divTag, json;
var url = APP_URL + "/controllers/dispatcher.php?command=";
var viewsurl = APP_URL + "/views";
$(document).ready(function () {
    $.get(url + "get_news",
        function success(data) {
            json = JSON.parse(data);

            $.each(json.data, function (key, value) {
                ShowIdNews(value);
            });
        });
});

function ShowIdNews(value) {
    divTag = $("#infoNews");
    aTag = $("<a>");
    aTag.attr("href", viewsurl + "/show_news.php?id=" + value.id);
    aTag.html(value.id + ' ' + value.category);
    divTag.append("<br>");
    divTag.append(aTag);
}
